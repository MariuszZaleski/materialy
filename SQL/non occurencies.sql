SELECT mail, 
 COUNT(mail) AS NumOccurrences
FROM client
GROUP BY mail
HAVING ( COUNT(mail) > 1 )
ORDER BY COUNT(mail)

SELECT	mail
FROM	client
GROUP BY mail
HAVING ( COUNT(mail) = 1 )
