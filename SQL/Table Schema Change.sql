DECLARE	@TableName nvarchar(200),
	@TableDescription varchar(1000),
	@ColumnName nvarchar(200),
	@ColumnDescription varchar(1000)

--* Schema Description
SELECT	@TableName =	'Booking',
	@TableDescription =	NULL,
	@ColumnName =	'AccommodationDeposit',
	@ColumnDescription ='Description: The portion of the accommodation deposit amount that has been calculated on a booking basis.' + CHAR(13) + CHAR(10) +
			'Nullable: Yes' + CHAR(13) + CHAR(10) +
			'Primary Key: No' + CHAR(13) + CHAR(10) +
			'Foreign Key: No'
EXEC dbo.spSchemaDescriptionAdd @TableName, @TableDescription, @ColumnName, @ColumnDescription